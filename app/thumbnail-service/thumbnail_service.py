
from concurrent import futures
from random import random
from time import time
from PIL import Image, ImageOps,ImageDraw, ImageFilter
from io import BytesIO
import base64
import logging

import grpc
import thumbnail_pb2_grpc
import thumbnail_pb2

PORT = '50051'

class Thumbnail(thumbnail_pb2_grpc.ThumbnailsImageServiceServicer):

    def thumbnailImage(self, request, context):
        with BytesIO() as output:
            # Build the mask
            offset = 0
            blur_radius = 4 
            size = (128, 128)

            offset = blur_radius * 2 + offset
            mask = Image.new("L", size, 0)
            draw = ImageDraw.Draw(mask)
            draw.ellipse((offset, offset, size[0] - offset, size[1] - offset), fill=255)
            mask = mask.filter(ImageFilter.GaussianBlur(blur_radius))

            request_id = str(int(time()))
            logging.info(f'Get request {request_id}')
            image_output = Image.open(BytesIO(base64.b64decode(request.b64image)))
            image_output = ImageOps.fit(image_output, mask.size, centering=(0.5, 0.5))
            image_output.putalpha(mask)
            image_output.save(output,'png')
            data = output.getvalue()
            logging.info(f'Finished {request_id}')


        return thumbnail_pb2.B64ImageOutput(b64image=data)

def serve():
    logging.info(f"Service started on port {PORT}...")
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    thumbnail_pb2_grpc.add_ThumbnailsImageServiceServicer_to_server(Thumbnail(), server)

    server.add_insecure_port(f'[::]:{PORT}')
    server.start()
    server.wait_for_termination()

if __name__ == '__main__':
    logging.basicConfig(filename='thumbnail.log', format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p',level=logging.INFO)
    serve()
