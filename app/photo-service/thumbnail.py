import grpc
from typing import List
import thumbnail_pb2, thumbnail_pb2_grpc

class ThumbnailClient:
    def __init__(self):
        self.channel = None
        self.stub = None

    def connect(self, service_host):
        self.channel = grpc.insecure_channel(service_host)
        self.stub = thumbnail_pb2_grpc.ThumbnailsImageServiceStub(self.channel)

